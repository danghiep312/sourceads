using System;
using System.Collections.Generic;
using UnityEngine;


public class NativeAdController : MonoBehaviour
{
    private int _requestCount;
    private Queue<Action> _requestQueue;
    private float _delayTime;


    private void Start()
    {
        _delayTime = 1f;
        _requestCount = 0;
        _requestQueue = new Queue<Action>();
        this.RegisterListener(EventID.RequestComplete, (o) => RequestComplete());
    }

    public void Register(Action callback)
    {
        Debug.Log("Register NativeAd" + StackTraceUtility.ExtractStackTrace());
        
        if (callback != null) _requestQueue.Enqueue(callback);
    }

    public void RequestAd()
    {
        if (_requestCount >= 4) return;
        if (_requestQueue.Count > 0)
        {
            _requestCount++;
            Action req = _requestQueue.Dequeue();
            req.Invoke();
        }
    }

    private void Update()
    {
        if (_delayTime > 0)
        {
            _delayTime -= Time.deltaTime;
        }
        else
        {
            if (Check())
                _delayTime = 1f;
        }
    }

    public void RequestComplete()
    {
        _requestCount--;
    }

    public bool Check()
    {
        if (_requestQueue.Count > 0)
        {
            RequestAd();
            return true;
        }

        return false;
    }
}
