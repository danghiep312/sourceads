#if UNITY_EDITOR
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor;
#endif
using UnityEngine;

public class PreBuildHandle : IPreprocessBuildWithReport
{
#if UNITY_EDITOR
    public int callbackOrder => 1;

    public void OnPreprocessBuild(BuildReport report)
    {
        var data = ScriptableObject.CreateInstance<BundleVer>();
        string versionCode = "1";
#if UNITY_ANDROID
        data.buildVersionCode = PlayerSettings.Android.bundleVersionCode.ToString();
#elif UNITY_IOS
        data.buildVersionCode = PlayerSettings.iOS.buildNumber;
#endif
        AssetDatabase.CreateAsset(data, "Assets/Source/Resources/BuildVer.asset");
        Debug.Log(data.buildVersionCode + " here");
        AssetDatabase.SaveAssets();
    }
#endif
}