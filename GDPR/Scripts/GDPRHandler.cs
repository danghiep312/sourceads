using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Ump;
using GoogleMobileAds.Ump.Api;
using System.Linq;
using System;


public class GDPRHandler : MonoBehaviour
{
    public static GDPRHandler Instance;

    private bool isFormReady;

    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }

#if ADMOB_UNITY_8_5_OR_NEWER
    void OnConsentInfoUpdated(FormError consentError)
    {
        if (consentError != null)
        {
            // Handle the error.
            InitLoadAds(true);
            Debug.LogError("Consent: " +consentError);
            return;
        }
        // If the error is null, the consent information state was updated.
        // You are now ready to check if a form is available.
        ConsentForm.LoadAndShowConsentFormIfRequired((FormError formError) =>
        {
            if (formError != null)
            {
                // Consent gathering failed.
                InitLoadAds(true);
                UnityEngine.Debug.LogError("Consent: Error " + consentError);
                return;
            }

            Debug.Log("Consent: No error");
            // Consent has been gathered.
            if (ConsentInformation.CanRequestAds()) // Ham nay chi dam bao la da hoan thanh dc CMP
            {
                Debug.LogError("Consent: START 03 : " + isCMPConsent());
                //Request an ad.
                InitLoadAds(isCMPConsent());
            }
            else
            {
                InitLoadAds(false);
            }
        });
    
    public void CheckReloadCMP()
        {
            {
                Debug.Log("Consent: Reload =>>> CanRequestAds " + ConsentInformation.CanRequestAds() + " isCMP: " +
                          isCMPConsent());
                if (!isCMPConsent())
                {
                    ResetCMP_Admod();
                }
            }
        }
#endif

#if ADMOB_UNITY_LOWER_8_5
    void OnConsentInfoUpdated(FormError consentError)
    {
        if (consentError != null)
        {
            // Handle the error.
            InitLoadAds(true);
            Debug.LogError("Consent: " + consentError);
            return;
        }
    }
#endif

        void Start()
        {
            Debug.Log("Consent: Start");
            // Check the current consent information status.
            if (isCMPConsent())
            {
                InitLoadAds(true);
            }
            else
            {
                ResetCMP_Admod();
            }
        }


        void InitLoadAds(bool isConsent)
        {
            Debug.Log("Consent: CMPConsent: " + isCMPConsent());
            MasterControl.Instance.adsController.Init();
            IronSource.Agent.setConsent(isConsent);
            // Các method có liên quan
        }


        

        public void ResetCMP_Admod()
        {
            ConsentInformation.Reset();
            Debug.Log("Consent: request");
            ConsentRequestParameters request = new ConsentRequestParameters
            {
                TagForUnderAgeOfConsent = false,
            };
            ConsentInformation.Update(request, OnConsentInfoUpdated);
        }

        bool isCMPConsent()
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                string CMPString = PlayerPrefsNative.GetString("IABTCF_AddtlConsent", "NO");
                if (CMPString != null)
                {
                    Debug.LogError("CMPString: " + CMPString);
                    if (CMPString.Contains("2878") || CMPString.Length >= 4)
                    {
                        return true;
                    }

                    return false;
                }

                return false;
            }

            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                string CMPString = PlayerPrefs.GetString("IABTCF_AddtlConsent", "NO");
                if (CMPString != null)
                {
                    Debug.LogError("CMPString: " + CMPString);

                    return CMPString.Contains("2878") || CMPString.Length >= 4;
                }

                return false;
            }

            return true;
        }
    }