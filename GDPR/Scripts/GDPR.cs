using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Ump.Api;
using UnityEngine;

public class GDPR : MonoBehaviour
{
    public static GDPR Instance;
    [SerializeField]
    private bool testMode = true;
    private List<String> testDevices = new List<string>();
    
    public static Action<bool> InitAdsCallback;
    [SerializeField] private bool tagForUnderAgeOfConsent;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }

        InitAdsCallback += b => { Time.timeScale = 1; };
    }

    public void InitGdpr()
    {
        Debug.Log("Consent : " + IsCmpConsent());
        if (IsCmpConsent())
        {
            InitAdsCallback?.Invoke(true);
        }
        else
        {
            ResetCMP_Admob();
        }
    }

    private void ResetCMP_Admob()
    {
        var debugSettings = new ConsentDebugSettings
        {
            DebugGeography = DebugGeography.EEA,
            TestDeviceHashedIds = testDevices

        };

        ConsentInformation.Reset();
        Debug.Log("Consent: request");
        ConsentRequestParameters request = new ConsentRequestParameters
        {
            // TagForUnderAgeOfConsent = tagForUnderAgeOfConsent,
            // ConsentDebugSettings = testMode ? debugSettings : null,
        };
        ConsentInformation.Update(request, OnConsentInfoUpdated);
    }
#if ADMOB_UNITY_LOWER_8_5
    
    private ConsentForm _consentForm;
    void OnConsentInfoUpdated(FormError error)
    {
        Debug.Log("Run info update");
        if (error != null)
        {
            // Handle the error.
            InitAdsCallback?.Invoke(true); 
            Debug.LogWarning("In update: " + error);
            Time.timeScale = 1;
            return;
        }

        if (ConsentInformation.IsConsentFormAvailable())
        {
            Debug.Log("CMP IsConsentFormAvailable = true, LoadConsentForm...");
            LoadConsentForm();
        }
        else
        {
            Debug.Log("CMP IsConsentFormAvailable = false, init ads...");
            InitAdsCallback?.Invoke(true);
        }
        
        // ConsentForm.Load((form, formError) =>
        // {
        //     Time.timeScale = 1;
        //     if (formError != null)
        //     {
        //         InitAdsCallback?.Invoke(true);
        //         Debug.LogError("Consent: Error " + formError);
        //         return;
        //     }
        //     bool consentStatus = IsCmpConsent();
        //     Debug.Log("Consent Form: Show Done, Consent : " + consentStatus);
        //     InitAdsCallback?.Invoke(consentStatus);
        // });
    }

    void LoadConsentForm()
    {
        Debug.Log("CMP LoadConsentForm...");
        Time.timeScale = 0; // pause to load form
        ConsentForm.Load(OnLoadConsentForm);
    }
    
    private void OnLoadConsentForm(ConsentForm consentForm, FormError error)
    {
        Debug.Log("CMP OnLoadConsentForm");

        if (error != null)
        {
            Debug.LogError("CMP LoadFormError" + error);
            InitAdsCallback?.Invoke(true);
            return;
        }

        // The consent form was loaded.
        // Save the consent form for future requests.
        _consentForm = consentForm;

        // You are now ready to show the form.
        if (ConsentInformation.ConsentStatus == ConsentStatus.Required)
        {
            Debug.Log("CMP Show Form Required...");
            _consentForm.Show(OnShowForm);
        }
        else
        {
            Debug.Log($"CMP ConsentStatus = {ConsentInformation.ConsentStatus}, InitLoadAds...");
            InitAdsCallback?.Invoke(true);
        }
        
    }
    
    private void OnShowForm(FormError error)
    {
        Debug.Log("CMP OnShowForm");

        if (error != null)
        {
            // Handle the error.
            Debug.LogError("CMP ShowFormError " + error);
            InitAdsCallback?.Invoke(true);
            return;
        }

        // Handle dismissal by reloading form.
        LoadConsentForm();
    }
#endif

#if ADMOB_UNITY_8_5_OR_NEWER

    void OnConsentInfoUpdated(FormError error)
    {
        // Debug.Log("Consent Status: " + ConsentInformation.ConsentStatus);
        if (error != null)
        {
            // Handle the error.
            InitAdsCallback?.Invoke(true); 
            Debug.LogWarning("In update: " + error);
            Time.timeScale = 1;
            return;
        }
        
        
        Time.timeScale = 0;
        ConsentForm.LoadAndShowConsentFormIfRequired(formError =>
        {
            Time.timeScale = 1;
            if (formError != null)
            {
                InitAdsCallback?.Invoke(true);
                Debug.LogError("Consent: Error " + formError);
                return;
            }
            
            bool consentStatus = ConsentInformation.CanRequestAds() && IsCmpConsent();
            Debug.Log("Consent Form: Show Done, Consent : " + consentStatus);
            InitAdsCallback?.Invoke(consentStatus);
        });
        
        
        // If the error is null, the consent information state was updated.
        // You are now ready to check if a form is available.
    }
#endif

    public static bool IsCmpConsent()
    {
        if (Instance.testMode) return false;
        return Application.platform switch
        {
            RuntimePlatform.Android => checkIABCTF(PlayerPrefsNative.GetString("IABTCF_AddtlConsent", "NO")),
            RuntimePlatform.IPhonePlayer => checkIABCTF(PlayerPrefs.GetString("IABTCF_AddtlConsent", "NO")),
            _ => true
        };

        bool checkIABCTF(string CMPString)
        {
            if (CMPString == null) return false;
            Debug.LogWarning("CMPString: " + CMPString);
            return CMPString.Contains("2878") || CMPString.Length >= 4;
        }
    }

}
