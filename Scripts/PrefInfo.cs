﻿using UnityEngine;

public class PrefInfo
{
    public static bool UseGdpr()
    {
        return PlayerPrefs.GetInt(SettingPattern.BlockGdpr, 0) == 0;
    }
    
    
    public static bool IsUsingAd()
    {
        return PlayerPrefs.GetInt("AdEnabled", 1) == 1;
    }
    public static void SetAd(bool active = false)
    {
        // lưu xem user đã mua chưa
        PlayerPrefs.SetInt("AdEnabled", active ? 1 : 0);
    }

    public static void SetTimeShowAds()
    {
        PlayerPrefs.SetString("TimeToShowAds", System.DateTime.Now.ToString());
    }
    
    public static System.DateTime GetTimeShowAds()
    {
        return System.DateTime.Parse(PlayerPrefs.GetString("TimeToShowAds", new System.DateTime(2020, 1, 1).ToString()));
    }

    public static void OpenGame()
    {
        PlayerPrefs.SetInt("OpenGame", NumberOfPlayTime() + 1);
    }

    public static int NumberOfPlayTime()
    {
        return PlayerPrefs.GetInt("OpenGame", 0);
    }
}