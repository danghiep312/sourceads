﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GoogleMobileAds.Api;
using Sirenix.OdinInspector;
using UnityEngine.Networking;
using UnityEngine.Purchasing;
using SourceAds;
using UnityEngine.Purchasing.Extension;

public class  MasterControl : MonoBehaviour
{
    public static MasterControl Instance;
    public static bool AllowShowAds = true;
    [HideInInspector]
    public AdsController adsController;
    private Purchaser purchaser;
    private FirebaseManager firebaseManager;
    private GDPR gdpr;
    [ReadOnly][ShowInInspector]
    //private List<IPluginReady> plugins = new List<IPluginReady>();
    private bool _isInit;

    public bool IsReady()
    {
        return adsController.IsReady();
    }

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            PrefInfo.OpenGame();
            
            adsController = GetComponentInChildren<AdsController>();
            purchaser = GetComponentInChildren<Purchaser>();
            firebaseManager = GetComponentInChildren<FirebaseManager>();
            gdpr = GetComponentInChildren<GDPR>();

            GDPR.InitAdsCallback += InitAds;
            FirebaseManager.OnFetchDataDone += gdpr.InitGdpr;
            FirebaseManager.OnFetchDataDone += adsController.LoadRule;
            
            MobileAds.RaiseAdEventsOnUnityMainThread = true;
            // plugins.Add(adsController.GetComponent<IPluginReady>());
            // plugins.Add(purchaser.GetComponent<IPluginReady>());
            // plugins.Add(firebaseManager.GetComponent<IPluginReady>());
#if !UNITY_IOS
            StartCoroutine(DoFindBestHost());
#endif
        }
        else {
            Destroy(gameObject);
        }
        Application.lowMemory += OnLowMemory;
        StartCoroutine(DelayInit());
    }

    

    private void OnLowMemory() {
        GC.Collect();
    }

    IEnumerator DelayInit()
    {
        yield return new WaitForSeconds(.5f);
        Init();
    }

    public void Init() {
        if (Instance._isInit) return;
        Instance._isInit = true;
        purchaser.Init();
        firebaseManager.Init();

    }
    bool initAds = false;
    
    public void InitAds()
    {
        if (initAds)
            return;
        initAds = true;
        Debug.Log("MC Init Ad");
        
        adsController.Init();
    }
    
    public void InitAds(bool consent)
    {
#if UNITY_IOS
        if (!(PlayerPrefs.GetInt("ATT", 0) == 1)) return;
#endif
        if (initAds)
            return;
        initAds = true;
        Debug.Log("MC Init Ad");
        
        adsController.Init();
        IronSource.Agent.setConsent(consent);
    }

    private IEnumerator WaitToInit()
    {
        // while (!_everythingIsReady)
        // {
        //     bool checkInit = plugins.All(p => p.IsReady());
        //     if (!checkInit) yield return null;
        //     _everythingIsReady = true;
        //     Common.Log("Everything is ready, let's go!");
        // }
        yield return null;
    }

    
    [Space]
    public AdsState adsState;
    #region IAP

    [Space]
    
    public string noAdsKey;
    [PropertyTooltip("Keys can be restored type = Non consumable")]
    public IAPKey[] productKeys;

    public void OnPurchased(UnityEngine.Purchasing.Product product) {
        OnPurchased(product.definition.id);
        Common.Log("PURCHASED: " + product.definition.id);
    }

    public void OnFailedToPurchase(UnityEngine.Purchasing.Product product, UnityEngine.Purchasing.PurchaseFailureReason reason) {
        

        if (reason.Equals(UnityEngine.Purchasing.PurchaseFailureReason.DuplicateTransaction)) {
            // Only exist with non consumable products.
            if (product != null) {
                if (product.definition.id.Equals(noAdsKey) && Purchaser.Instance.HasReceipt(product.definition.id)) {
                    OnPurchased(product.definition.id);
                }
            }
        }
        else
        {
            // do something with other product
        }
    }
    
    public void OnFailedToPurchase(Product product, PurchaseFailureDescription reason) {
        

        if (reason.reason.Equals(PurchaseFailureReason.DuplicateTransaction)) {
            // Only exist with non consumable products.
            if (product != null) {
                if (product.definition.id.Equals(noAdsKey) && Purchaser.Instance.HasReceipt(product.definition.id)) {
                    OnPurchased(product.definition.id);
                }
            }
        }
        else
        {
            // do something with other product
        }
    }

    public void OnPurchased(string item) {
        Common.Log("MASTER CONTROL: ON PURCHASED " + item);
        if (item.Equals(noAdsKey)) {
            PrefInfo.SetAd(false);
            HideBanner();
            //Home.Instance.noAdsBtn.SetActive(false);
            //GameUI.Instance.CheckAd();
            //MessagePanel.Instance.SetUp(Wugner.Localize.Localization.GetEntry(null, Loc.ID.Message.RemoveAdsSuccess).Content, Wugner.Localize.Localization.GetEntry(null, Loc.ID.Common.MessageText).Content);
            FirebaseManager.Instance.LogEvent("PURCHASE_SUCCESS_NOADS");
        }
        else
        {
            // do something with other product
        }
    }

    public void OnRestore(string item) {
        Debug.Log("<color=lime> MasterControl: OnRestore </color>" + item);

        if (item.Equals(noAdsKey)) {
            PrefInfo.SetAd(false);
            this.HideBanner();
        }
    }

    public void CheckRestore()
    {
        purchaser.CheckRestore();
    }
    
    #endregion


    public void OpenURL(string link) {
        Application.OpenURL(link);
    }

    #region AD

    public bool ShowOpenAd(Action<bool> callback = null)
    {
        if (!AllowShowAds)
        {
            Common.Log("Off Ad In Remote Config");
            callback?.Invoke(true);
            return false;
        }

        if (RemoteConfigPattern.UseNativeClick)
        {
            if (NativeAdComponent.ClickNative)
            {
                NativeAdComponent.ClickNative = false;
                Common.Log("Off Native Click Remote Config");
                callback?.Invoke(true);
                return false;
            }
        }
        
        bool res = adsController.ShowOpenAppAds(callback);
        if (!res) callback?.Invoke(true);
        return res;
    }
        
    
    public void ShowRewardedAd(Action<bool> rewardCallBack = null, bool checkNoAds = false) {
        if (adsState == AdsState.UnlockAll || !AllowShowAds)
        {
            rewardCallBack?.Invoke(true);
            Debug.Log("Unlock ad or No ad Remote Config");
            return;
        }

        if (!PrefInfo.IsUsingAd())
        {
            if (!checkNoAds)
            {
                rewardCallBack?.Invoke(true);
                return;
            }
        }
        // check no ads = true if user purchased no ads but still show ads
        if (!CheckInternetSetting()) {
            return;
            // UINoInternetPanel.instance.SetUp();
        }
        
        adsController.ShowReward(rewardCallBack);
    }

    public bool IsRewardAdAvailable()
    {
        return adsController.IsRewardAdAvailable();
    }

    public bool ShowInterstitialAd(bool allowDelay = false, Action<bool> callback = null) {
        if (!PrefInfo.IsUsingAd() || adsState == AdsState.UnlockAll || !AllowShowAds)
        {
            callback?.Invoke(true);
            Debug.Log("Unlock version");
            return true;
        }
        return adsController.ShowInterstitial(allowDelay, callback);
    }
    
    public bool IsInterstitialAdAvailable()
    {
        return adsController.IsReadyShowInterstitial();
    }
    
    
    public bool IsAllowShowAdLoadingPanel(AdType type)
    {
        if (type == AdType.Interstitial)
            return adsController.IsAllowShowInterstitial() && adsController.IsReadyShowInterstitial();
        else if (type == AdType.Reward)
            return IsRewardAdAvailable();
        else
        {
            // open app ad
            return true;
        }
    }

    public void ShowBanner() {
        if (!PrefInfo.IsUsingAd() || !AllowShowAds || adsState == AdsState.UnlockAll)
        {
            Common.Log("Unlock version or no ads remote config");
            return;
        }

        adsController.ShowCollapsibleBanner();
    }
    
    public void HideBanner() {
        adsController.HideCollapsibleBanner();
    }

    private bool CheckInternetSetting()
    {
        return Application.internetReachability != NetworkReachability.NotReachable;
    }

    #endregion

    #region Internet and bundle installation check

    public bool CheckAppInstallation(string bundleId) {
#if UNITY_EDITOR
        return false;
#elif UNITY_ANDROID
        bool installed = false;
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject curActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = curActivity.Call<AndroidJavaObject>("getPackageManager");

        AndroidJavaObject launchIntent = null;
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
            if (launchIntent == null)
                installed = false;
            else
                installed = true;
        }
        catch (System.Exception e)
        {
            installed = false;
        }
        return installed;

#elif UNITY_IOS
        return false;
#else
        return false;
#endif
    }

    public void CheckInternet() {
#if !UNITY_IOS
        CheckInternet(res =>
        {
            
        }, false);
#endif
    }

    public void CheckInternet(Action<bool> action, bool useWaitingUI = false) {
#if !UNITY_IOS
        StartCoroutine(DoCheckInternet(action, useWaitingUI));
#endif
    }

    private string[] hosts = { "8.8.4.4", "8.8.8.8", "180.76.76.76", "1.1.1.1" };
    private string bestHost = "8.8.4.4";
    IEnumerator DoFindBestHost() {
        int pingTime = int.MaxValue;
        for (int i = 0; i < hosts.Length; i++) {
            Ping ping = new Ping(hosts[i]);

            float timeOutCooldown = 0;
            while (!ping.isDone && timeOutCooldown < 5) {
                yield return null;
                timeOutCooldown += Time.unscaledDeltaTime;
            }

            if (ping.isDone && pingTime > ping.time) {
                pingTime = ping.time;
                bestHost = hosts[i];
            }
        }
        Debug.Log("Ping Host: " + bestHost);
    }
    
    IEnumerator DoCheckInternet(Action<bool> action, bool useWaitingUI = false) {
        yield return new WaitForSecondsRealtime(2);
        if (Application.internetReachability.Equals(NetworkReachability.NotReachable)) {
            action(false);
            yield break;
        }
        Ping ping = new Ping(bestHost);

        float timeOutCooldown = 0;
        bool check = false;
        while (!ping.isDone && timeOutCooldown < 5) {
            yield return null;
            timeOutCooldown += Time.unscaledDeltaTime;
            if (useWaitingUI) {
                if (!check && timeOutCooldown > 0.3f) {
                    check = true;
                    //Controller.Instance.pleaseWaitPanel.SetActive(true);
                }
            }
        }
        Debug.Log("IS DONE: " + ping.isDone + " " + ping.time + " " + bestHost);
        if (ping.isDone) {
            //isConnected = true;
            StartCoroutine(checkInternetConnection(action, useWaitingUI));
            //action(true);
        }
        else {
            action(false);
        }
    }
    
    public IEnumerator checkInternetConnection(Action<bool> action, bool useWaitingUI) {
        var www = new UnityWebRequest("https://google.com");

        bool check = false;
        float timeOut = 5;
        while (!www.isDone && timeOut > 0) {
            if (www.downloadedBytes >= 2) {
                break;
            }
            if (!check && useWaitingUI && timeOut < 4.7f) {
                check = true;
                //Controller.Instance.pleaseWaitPanel.SetActive(true);
            }

            timeOut -= Time.deltaTime;
            yield return null;
        }

        //yield return www;
        Debug.Log("1: " + www.downloadedBytes);
        if (www.downloadedBytes < 1) {
            var www2 = new UnityWebRequest("https://www.baidu.com/");
            timeOut = 5;
            while (!www2.isDone && timeOut > 0) {
                if (www2.downloadedBytes >= 2) {
                    break;
                }

                timeOut -= Time.deltaTime;
                yield return null;
            }
            Debug.Log("2 : " + www2.downloadedBytes);

            if (www2.downloadedBytes < 1) {
                var www3 = new UnityWebRequest("https://www.baidu.com/");
                yield return www3;
                Debug.Log("3 : " + www3.downloadedBytes);

                if (www3.downloadedBytes < 1) {
                    UnityWebRequest www4 = new UnityWebRequest("https://worldclockapi.com/api/json/utc/now");
                    yield return www4;
                    Debug.Log("4 : " + www4.downloadedBytes);

                    if (www4.downloadedBytes < 1) {
                        action(false);
                    }
                    else {
                        action(true);
                    }
                }
                else {
                    action(true);
                }
            }
            else {
                action(true);
            }
        }
        else {
            action(true);
        }
    }

    #endregion
}

[Serializable]
public class IAPKey
{
    public string key;
    public ProductType type;
}

public enum AdType
{
    Banner = 0,
    Interstitial = 1,
    Reward = 2,
    Native = 3,
    OpenApp = 4,
}