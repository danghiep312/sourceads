﻿using UnityEngine.Purchasing;


namespace SourceAds
{
    using UnityEngine;

    public class MasterControlListener : MonoBehaviour
    {
        public void OnPurchased(Product product)
        {
            MasterControl.Instance.OnPurchased(product);
        }

        public void OnFailedToPurchase(Product product,
            PurchaseFailureReason reason)
        {
            MasterControl.Instance.OnFailedToPurchase(product, reason);
        }

        public void OnFailedToPurchase(Product product, UnityEngine.Purchasing.Extension.PurchaseFailureDescription reason)
        {
            MasterControl.Instance.OnFailedToPurchase(product, reason);
        }

        public void OpenURL(string link)
        {
            MasterControl.Instance.OpenURL(link);
        }

        public static void Restore()
        {
            Purchaser.Instance.CheckRestore();
        }

    }

}