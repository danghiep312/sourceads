using Cysharp.Threading.Tasks;

namespace SourceAds
{
    using System;
    using Firebase;
    using UnityEngine;
    using Firebase.Analytics;
    using System.Collections;
    using Firebase.RemoteConfig;
    using Sirenix.OdinInspector;

    public class FirebaseManager : MonoBehaviour
    {
        public static FirebaseManager Instance { get; set; }
        [ReadOnly] public bool isReady = false;
        
        public static Action OnFetchDataDone;

        // Use this for initialization
        DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
        
        
        bool _checkFetchDone = false;
        LastFetchStatus result;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            FetchData();
        }

        public void Init()
        {
            DelayInit();
        }
        
        public void FetchRemoteConfig()
        {
            Debug.Log("Fetching data...");
            Debug.LogFormat("Firebase init 3");
            try
            {
                FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero).ContinueWith(task2 =>
                {

                    ConfigInfo info = FirebaseRemoteConfig.DefaultInstance.Info;
                    result = info.LastFetchStatus;
                    if (info.LastFetchStatus.Equals(LastFetchStatus.Success))
                    {
                        _checkFetchDone = true;
                    }
                    else if (info.LastFetchStatus.Equals(LastFetchStatus.Failure))
                    {
                        Debug.Log("FAIL TO LOAD");
                    }
                    else
                    {
                        Debug.Log("PENDING");
                    }
                });
                Debug.LogFormat("Firebase init 4");
            }
            catch (Exception e)
            {
                Debug.Log("LOI 5108410284: " + e.ToString());
            }
        }


        void DelayInit()
        {
            isReady = false;

            Debug.Log("INIT FIREBASE");
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
            {
                Debug.Log("FIREBASE INITIALIZED");
                dependencyStatus = task.Result;
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
                else
                {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }

        private void InitializeFirebase()
        {
            Debug.Log("Enabling data collection.");
            FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
            isReady = true;
        }

        private async void WaitForFetch()
        {
            await UniTask.WaitUntil(() => isReady, PlayerLoopTiming.EarlyUpdate);
            FetchRemoteConfig();
        }

        public void FetchData()
        {
            _checkFetchDone = false;
            WaitForFetch();
            DoWaitForFetchDone();
        }
        
        private async void DoWaitForFetchDone()
        {
            await UniTask.WaitUntil(() => _checkFetchDone);

            if (result.Equals(LastFetchStatus.Success))
            {
                Debug.Log("Start Fetch Remote Config");
                try
                {
                    RemoteConfigPattern.FetchData();
                    OnFetchDataDone?.Invoke();
                }
                catch (Exception e)
                {
                    Debug.LogError(e.ToString());
                }
            }
            else if (result.Equals(LastFetchStatus.Failure))
            {
                Debug.Log("FAIL TO LOAD");
            }
            else
            {
                Debug.Log("PENDING");
            }
        }

        

        public void LogEvent(string eventName)
        {
            FirebaseAnalytics.LogEvent(eventName);
        }
    }
}