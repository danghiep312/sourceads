using Firebase.RemoteConfig;
using UnityEngine;


public class RemoteConfigPattern
{
    public static bool UseNativeClick = false;
    
    
    public static void FetchData()
    {
        FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
        var remoteConfig = FirebaseRemoteConfig.DefaultInstance;

        ConfigValue blockDebug = remoteConfig.GetValue(SettingPattern.BlockDebug);
        Debug.Log("Debug: " + blockDebug.StringValue);
        if (!string.IsNullOrEmpty(blockDebug.StringValue))
        {
            PlayerPrefs.SetInt(SettingPattern.BlockDebug, blockDebug.BooleanValue ? 1 : 0);
        }


        ConfigValue value1 = remoteConfig.GetValue(AdPattern.AdSettingTimeReward);
        Debug.Log("AdSetting_time_reward: " + value1.StringValue);
        if (!string.IsNullOrEmpty(value1.StringValue))
        {
            PlayerPrefs.SetInt(AdPattern.AdSettingTimeReward, int.Parse(value1.StringValue));
        }

        
        ConfigValue value2 = remoteConfig.GetValue(AdPattern.AdSettingTimeNormal);
        Debug.Log("AdSetting_time_normal: " + value2.StringValue);
        if (!string.IsNullOrEmpty(value2.StringValue))
        {
            PlayerPrefs.SetString(AdPattern.AdSettingTimeReward, value2.StringValue);
        }

        
        ConfigValue value3 = remoteConfig.GetValue(AdPattern.AdSettingTimeInterstitial);
        Debug.Log("AdSetting_time_reward" + value3.StringValue);
        if (!string.IsNullOrEmpty(value3.StringValue))
        {
            PlayerPrefs.SetInt(AdPattern.AdSettingTimeInterstitial, int.Parse(value3.StringValue));
        }

        
        ConfigValue value4 = remoteConfig.GetValue(AdPattern.AdSettingLevel);
        Debug.Log("AdSetting_level: " + value4.StringValue);
        if (!string.IsNullOrEmpty(value4.StringValue))
        {
            PlayerPrefs.SetString(AdPattern.AdSettingLevel, value4.StringValue);
        }

        var ver = Resources.Load<BundleVer>("BuildVer");
        ConfigValue blockAds = remoteConfig.GetValue(AdPattern.AdSettingBlock + ver.buildVersionCode);
        Debug.Log($"AdSetting_block_" + ver.buildVersionCode + ": " + blockAds.StringValue);
        MasterControl.AllowShowAds = !blockAds.BooleanValue;
        


        ConfigValue useClickNative = remoteConfig.GetValue(SettingPattern.UseNativeClick);
        Debug.Log("Use native click: " + useClickNative.StringValue);
        UseNativeClick = useClickNative.BooleanValue;
        
        
        ConfigValue excludeId = remoteConfig.GetValue(CustomPattern.ExcludeIdMonster);
        Debug.Log("Exclude_id_monster: " + excludeId.StringValue.Replace(" ", ""));
        if (!string.IsNullOrEmpty(excludeId.StringValue))
        {
            PlayerPrefs.SetString(CustomPattern.ExcludeIdMonster, excludeId.StringValue.Replace(" ", ""));
        }
        
        ConfigValue blockGdpr = remoteConfig.GetValue(SettingPattern.BlockGdpr);
        Debug.Log("Block_gdpr: " + blockGdpr.StringValue);
        if (!string.IsNullOrEmpty(blockGdpr.StringValue))
        {
            PlayerPrefs.SetInt(SettingPattern.BlockGdpr, blockGdpr.BooleanValue ? 1 : 0);
        }
    }
}

public class AdPattern
{
    public static string AdSettingTimeReward = "AdSetting_time_reward";
    public static int AdSettingTimeRewardDefault = 10;
    
    public static string AdSettingTimeInterstitial = "AdSetting_time_interstitial";
    public static int AdSettingTimeInterstitialDefault = 15;

    public static string AdSettingLevel = "AdSetting_level";
    public static string AdSettingLevelDefault = "3,15,30,60,100";

    public static string AdSettingTimeNormal = "AdSetting_time_normal";
    public static string AdSettingTimeNormalDefault = "60,55,50,48,45";
        
    public static string AdSettingBlock = "AdSetting_block_";
}

public class SettingPattern
{
    public static string BlockDebug = "Block_debug";
    public static string UseNativeClick = "Use_native_click";
    public static string BlockGdpr = "Block_gdpr";
    // to block show open ads when click ad native
}

public class CustomPattern
{
    public static string ExcludeIdMonster = "Exclude_id_monster";
}
