﻿using System.Collections.Generic;
#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

namespace SourceAds
{
    using System;
    using System.Collections;
    using com.adjust.sdk;
    using Firebase.Analytics;
    using GoogleMobileAds.Api;
    using GoogleMobileAds.Common;

    using UnityEngine;
    using UnityEngine.Serialization;

    public class IronSourceManager : MonoBehaviour, IAdsController
    {
        public bool IsReady()
        {
            return _isReady;
        }

        //[Tooltip("User Id is used when using server-to-server callbacks. Ex: Offerwall, ..")]
        //public string uniqueUserId = "demoUserUnity";
        // public string INTERSTITIAL_INSTANCE_ID = "0";
        // public string REWARDED_INSTANCE_ID = "0";
        private AdsController _adsController;
        private string _appKey;

        public IAdsController next;
        private bool _isReady;

        private WaitForSeconds _waitTime = new WaitForSeconds(1f);
        Queue<IronSourceImpressionData> _impressionDataQueue = new Queue<IronSourceImpressionData>();
        #region init

        private void OnApplicationPause(bool isPaused)
        {
            Common.Log("unity-script: OnApplicationPause = " + isPaused);
            IronSource.Agent.onApplicationPause(isPaused);
        }

        public void Init(AdsController ctr, Action initCallback)
        {
            Debug.Log("unity-script: MyAppStart Start called");
            _adsController = ctr;

            var developerSettings =
                Resources.Load<IronSourceMediationSettings>(IronSourceConstants.IRONSOURCE_MEDIATION_SETTING_NAME);

            if (developerSettings == null)
            {
                Debug.Log("Mediation Settings are not available!");
                return;
            }

            _appKey = developerSettings.AndroidAppKey;
#if UNITY_ANDROID
            _appKey = developerSettings.AndroidAppKey;
#elif UNITY_IOS
            _appKey = developerSettings.IOSAppKey;
#endif

            IronSourceEvents.onSdkInitializationCompletedEvent -= InitializationCompleteEvent;
            IronSourceEvents.onSdkInitializationCompletedEvent += InitializationCompleteEvent;
            IronSourceEvents.onImpressionDataReadyEvent -= ImpressionDataReadyEvent;
            IronSourceEvents.onImpressionDataReadyEvent += ImpressionDataReadyEvent;

            IronSourceConfig.Instance.setClientSideCallbacks(true);

#if UNITY_IOS
        // Set the flag as true 
        AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
        // Set the flag as false
        //AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(false);
#endif

            if (_appKey.Equals(string.Empty))
            {
                Debug.LogWarning("IronSourceInitilizer Cannot init without AppKey");
            }
            else
            {
                IronSource.Agent.init(_appKey);
                Debug.Log($"{_appKey}, ISVersion: {IronSource.pluginVersion()}");
            }

            Debug.Log("Adapter debug " + developerSettings.EnableAdapterDebug);
            if (developerSettings.EnableAdapterDebug)
            {
                IronSource.Agent.setAdaptersDebug(true);
            }

            Debug.Log("EnableIntegrationHelper " + developerSettings.EnableIntegrationHelper);
            if (developerSettings.EnableIntegrationHelper)
            {
                IronSource.Agent.validateIntegration();
                Debug.Log("unity-script: IronSource.Agent.validateIntegration");
            }

            var id = IronSource.Agent.getAdvertiserId();
            Debug.Log("unity-script: IronSource.Agent.getAdvertiserId : " + id);


            IronSource.Agent.shouldTrackNetworkState(true);
            // IronSource.Agent.setConsent(true);
            
            ISAdQualityConfig isAdQualityConfig = new ISAdQualityConfig();
            IronSourceAdQuality.Initialize(_appKey, isAdQualityConfig);

            PreloadAds();
            initCallback?.Invoke();
            
        }

        private void InitializationCompleteEvent()
        {
            Debug.Log("Init iron source complete");
            _isReady = true;
            PreloadAds();
        }

        private void PreloadAds()
        {
            UnloadRewardEvents();
            UnloadBannerEvents();
            UnloadInterEvents();
            
            LoadBannerEvents();
            LoadInterstitialEvents();
            LoadRewardEvents();
            //LoadBanner();

            Debug.Log("unity-script: IronSource.Agent.Initialized");
            if (PrefInfo.IsUsingAd() && MasterControl.Instance.adsState == AdsState.AdsProduction)
            {
                LoadInterstitial();
                //TODO: Do something here
            }
        }

        #endregion
        
        void SendImpressionData()
        {
            while (_impressionDataQueue.Count > 0)
            {
                IronSourceImpressionData data = _impressionDataQueue.Dequeue();
                SendRevenueIronSourceToFirebase(data);
                SendRevenueToIronSourceAdjust(data);
            }
        }

        #region banner

        public void LoadBanner(BannerType type)
        {
            Debug.Log("Trying load banner");
            if (type == BannerType.BANNER)
                IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            else if (type == BannerType.LARGE)
                IronSource.Agent.loadBanner(IronSourceBannerSize.LARGE, IronSourceBannerPosition.BOTTOM);
            else
                IronSource.Agent.loadBanner(IronSourceBannerSize.SMART, IronSourceBannerPosition.BOTTOM);
        }
        
        public bool ShowBanner()
        {
            Debug.Log("SHOW BANNER ");
            IronSource.Agent.displayBanner();
            return true;
        }
        
        public void HideBanner()
        {
            try
            {
                IronSource.Agent.hideBanner();
            }
            catch
            {
                // ignored
            }
        }

        private void LoadBannerEvents()
        {
            //Add AdInfo Banner Events
            IronSourceBannerEvents.onAdLoadedEvent += BannerOnAdLoadedEvent;
            IronSourceBannerEvents.onAdLoadFailedEvent += BannerOnAdLoadFailedEvent;
            IronSourceBannerEvents.onAdClickedEvent += BannerOnAdClickedEvent;
            IronSourceBannerEvents.onAdScreenPresentedEvent += BannerOnAdScreenPresentedEvent;
            IronSourceBannerEvents.onAdScreenDismissedEvent += BannerOnAdScreenDismissedEvent;
            IronSourceBannerEvents.onAdLeftApplicationEvent += BannerOnAdLeftApplicationEvent;
        }

        private void UnloadBannerEvents()
        {
            try
            {
                IronSourceBannerEvents.onAdLoadedEvent -= BannerOnAdLoadedEvent;
            }
            catch (Exception e)
            {
                //Debug.Log(e);
            }
            IronSourceBannerEvents.onAdLoadFailedEvent -= BannerOnAdLoadFailedEvent;
            IronSourceBannerEvents.onAdClickedEvent -= BannerOnAdClickedEvent;
            IronSourceBannerEvents.onAdScreenPresentedEvent -= BannerOnAdScreenPresentedEvent;
            IronSourceBannerEvents.onAdScreenDismissedEvent -= BannerOnAdScreenDismissedEvent;
            IronSourceBannerEvents.onAdLeftApplicationEvent -= BannerOnAdLeftApplicationEvent;
        }

        //Banner Events
        private void BannerOnAdLoadedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got BannerAdLoadedEvent");
            BannerEvents.OnAdLoaded();
        }

        private void BannerOnAdLoadFailedEvent(IronSourceError error)
        {
            //masterAdsController.currentLoadedBannerCtr = null;
            //masterAdsController.failedBanner = true;
            Debug.Log("unity-script: I got BannerAdLoadFailedEvent, code: " + error.getCode() + ", description : " +
                      error.getDescription());
            BannerEvents.OnAdLoadFailed();
        }

        private void BannerOnAdClickedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got BannerAdClickedEvent");
            BannerEvents.OnAdClicked();
        }

        private void BannerOnAdScreenPresentedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got BannerAdScreenPresentedEvent");
            BannerEvents.OnAdScreenPresented();
        }

        private void BannerOnAdScreenDismissedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got BannerAdScreenDismissedEvent");
            BannerEvents.OnAdScreenDismissed();
        }

        private void BannerOnAdLeftApplicationEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got BannerAdLeftApplicationEvent");
            BannerEvents.OnAdLeftApplication();
        }

        #endregion


        #region interstitial

        private Action<bool> _onShowInterstitial;

        public void LoadInterstitial()
        {
            Debug.Log("unity-script: LoadInterstitial");
            IronSource.Agent.loadInterstitial();
        }

        public bool ShowInterstitial(Action<bool> onShow)
        {
            _onShowInterstitial = onShow;
            if (IronSource.Agent.isInterstitialReady())
            {
                IronSource.Agent.showInterstitial();
                return true;
            }

            //Debug.Log("unity-script: IronSource.Agent.isInterstitialReady - False");
            return false;
        }

        public bool IsInterstitialReady()
        {
            return IronSource.Agent.isInterstitialReady();
        }

        public void LoadInterstitialEvents()
        {
            //Add AdInfo Interstitial Events
            IronSourceInterstitialEvents.onAdReadyEvent += InterstitialOnAdReadyEvent;
            IronSourceInterstitialEvents.onAdLoadFailedEvent += InterstitialOnAdLoadFailed;
            IronSourceInterstitialEvents.onAdOpenedEvent += InterstitialOnAdOpenedEvent;
            IronSourceInterstitialEvents.onAdClickedEvent += InterstitialOnAdClickedEvent;
            IronSourceInterstitialEvents.onAdShowSucceededEvent += InterstitialOnAdShowSucceededEvent;
            IronSourceInterstitialEvents.onAdShowFailedEvent += InterstitialOnAdShowFailedEvent;
            IronSourceInterstitialEvents.onAdClosedEvent += InterstitialOnAdClosedEvent;

        }

        public void UnloadInterEvents()
        {
            // Add Interstitial Events
            IronSourceInterstitialEvents.onAdReadyEvent -= InterstitialOnAdReadyEvent;
            IronSourceInterstitialEvents.onAdLoadFailedEvent -= InterstitialOnAdLoadFailed;
            IronSourceInterstitialEvents.onAdOpenedEvent -= InterstitialOnAdOpenedEvent;
            IronSourceInterstitialEvents.onAdClickedEvent -= InterstitialOnAdClickedEvent;
            IronSourceInterstitialEvents.onAdShowSucceededEvent -= InterstitialOnAdShowSucceededEvent;
            IronSourceInterstitialEvents.onAdShowFailedEvent -= InterstitialOnAdShowFailedEvent;
            IronSourceInterstitialEvents.onAdClosedEvent -= InterstitialOnAdClosedEvent;
        }



        /************* Interstitial Events *************/
        private void InterstitialOnAdReadyEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdReadyEvent");
            InterstitialEvents.OnAdReady();
        }

        private void InterstitialOnAdLoadFailed(IronSourceError error)
        {
            Debug.Log("unity-script: I got InterstitialAdLoadFailedEvent, code: " + error.getCode() +
                      ", description : " +
                      error.getDescription());
            InterstitialEvents.OnAdLoadFailed();
            StartCoroutine(WaitToReload());
            // masterAdsController.InterstitialCallback(false);
        }

        private void InterstitialOnAdShowSucceededEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdShowSucceededEvent");
            _onShowInterstitial?.Invoke(true);
            _onShowInterstitial = null;
            SendImpressionData();
            InterstitialEvents.OnAdShowSucceeded();
        }

        private void InterstitialOnAdShowFailedEvent(IronSourceError error, IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdShowFailedEvent, code :  " + error.getCode() +
                      ", description : " +
                      error.getDescription());
            _onShowInterstitial?.Invoke(false);
            _onShowInterstitial = null;
            InterstitialEvents.OnAdShowFailed();
        }

        private void InterstitialOnAdClickedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdClickedEvent");
            InterstitialEvents.OnAdClicked();
        }

        private void InterstitialOnAdOpenedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdOpenedEvent");
            InterstitialEvents.OnAdOpened();
        }

        private void InterstitialOnAdClosedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got InterstitialAdClosedEvent");
            InterstitialEvents.OnAdClosed();
        }


        #endregion


        #region reward

        private Action<bool> _onRewarded;

        public void LoadRewardedVideo()
        {
            Debug.Log("Load reward video");
            // LoadRewardEvent();
        }

        public bool ShowRewardedVideo(Action<bool> onRewardedCallback)
        {
            _onRewarded = onRewardedCallback;
            if (IronSource.Agent.isRewardedVideoAvailable())
            {
                IronSource.Agent.showRewardedVideo();
                return true;
            }
            return false;
        }

        public void OnRewarded()
        {
            Debug.Log("ON REWARDED");
            SendImpressionData();
            _onRewarded?.Invoke(true);
            _onRewarded = null;
        }

        public void LoadRewardEvents()
        {
            //Add AdInfo Rewarded Video Events
            IronSourceRewardedVideoEvents.onAdOpenedEvent += RewardedVideoOnAdOpenedEvent;
            IronSourceRewardedVideoEvents.onAdClosedEvent += RewardedVideoOnAdClosedEvent;
            IronSourceRewardedVideoEvents.onAdAvailableEvent += RewardedVideoOnAdAvailable;
            IronSourceRewardedVideoEvents.onAdUnavailableEvent += RewardedVideoOnAdUnavailable;
            IronSourceRewardedVideoEvents.onAdShowFailedEvent += RewardedVideoOnAdShowFailedEvent;
            IronSourceRewardedVideoEvents.onAdRewardedEvent += RewardedVideoOnAdRewardedEvent;
            IronSourceRewardedVideoEvents.onAdClickedEvent += RewardedVideoOnAdClickedEvent;
        }

        public void UnloadRewardEvents()
        {
            IronSourceRewardedVideoEvents.onAdOpenedEvent -= RewardedVideoOnAdOpenedEvent;
            IronSourceRewardedVideoEvents.onAdClosedEvent -= RewardedVideoOnAdClosedEvent;
            IronSourceRewardedVideoEvents.onAdAvailableEvent -= RewardedVideoOnAdAvailable;
            IronSourceRewardedVideoEvents.onAdUnavailableEvent -= RewardedVideoOnAdUnavailable;
            IronSourceRewardedVideoEvents.onAdShowFailedEvent -= RewardedVideoOnAdShowFailedEvent;
            IronSourceRewardedVideoEvents.onAdRewardedEvent -= RewardedVideoOnAdRewardedEvent;
            IronSourceRewardedVideoEvents.onAdClickedEvent -= RewardedVideoOnAdClickedEvent;
        }


        /************* RewardedVideo Delegates *************/
        private void RewardedVideoOnAdAvailable(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoOnAdAvailable");
            RewardEvents.OnAdAvailable();
            // masterAdsController.currentLoadedRewardCtr = this;
            // adsController.RewardedVideoOnAdAvailable();
        }

        private void RewardedVideoOnAdUnavailable()
        {
            Debug.Log("unity-script: I got RewardedVideoOnAdUnavailable");
            RewardEvents.OnAdUnavailable();
        }

        private void RewardedVideoOnAdOpenedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoAdOpenedEvent");
            RewardEvents.OnAdOpened();
        }

        private void RewardedVideoOnAdClosedEvent(IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoAdClosedEvent");
            RewardEvents.OnAdClosed();
        }

        private void RewardedVideoOnAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoAdRewardedEvent, amount = " + placement.getRewardAmount() +
                      " name = " +
                      placement.getRewardName());
            RewardEvents.OnAdRewarded();
            OnRewarded();
        }

        private void RewardedVideoOnAdShowFailedEvent(IronSourceError error, IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoAdShowFailedEvent, code :  " + error.getCode() +
                      ", description : " + error.getDescription());
            _onRewarded?.Invoke(false);
            _onRewarded = null;
            RewardEvents.OnAdShowFailed();
            // UnloadRewardEvent();
            // LoadRewardedVideo();
        }

        private void RewardedVideoOnAdClickedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo)
        {
            Debug.Log("unity-script: I got RewardedVideoAdClickedEvent, name = " + placement.getRewardName());
            RewardEvents.OnAdClicked();
        }

        public bool IsRewardedVideoReady()
        {
            return IronSource.Agent.isRewardedVideoAvailable();
        }

        #endregion


        public void SetNext(IAdsController ctr)
        {
            next = ctr;
        }

        public static bool HasInternet()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log("Error. Check internet connection!");
                return false;
            }

            return true;
        }

        #region ===============EVENT ADREVENUE IRONSOURCE==============

        void ImpressionDataReadyEvent(IronSourceImpressionData impressionData)
        {
            _adsController.AddImpressionDataIronSource(impressionData);
            if (impressionData.adUnit.Equals("banner"))
            {
                SendRevenueIronSourceToFirebase(impressionData);
                SendRevenueToIronSourceAdjust(impressionData);
            }
            else
            {
                _impressionDataQueue.Enqueue(impressionData);
            }
        }


        private void SendRevenueIronSourceToFirebase(IronSourceImpressionData impressionData)
        {
            if (impressionData == null) return;
            double revenue = (double)impressionData.revenue;
            Parameter[] adParameters =
            {
                new Parameter("ad_platform", "ironSource"),
                new Parameter("ad_source", impressionData.adNetwork),
                new Parameter("ad_unit_name", impressionData.adUnit),
                new Parameter("ad_format", impressionData.instanceName),
                new Parameter("currency", "USD"),
                new Parameter("value", revenue)
            };
            FirebaseAnalytics.LogEvent("ad_impression", adParameters);
            Debug.Log($"EVENT IronSource ad_impression: " + impressionData);
        }


        private void SendRevenueToIronSourceAdjust(IronSourceImpressionData impressionData)
        {
            if (impressionData == null) return;
            double revenue = (double)impressionData.revenue;
            AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
            adjustAdRevenue.setRevenue(revenue, "USD");
// optional fields
            adjustAdRevenue.setAdRevenueNetwork(impressionData.adNetwork);
            adjustAdRevenue.setAdRevenueUnit(impressionData.adUnit);
            adjustAdRevenue.setAdRevenuePlacement(impressionData.placement);
// track Adjust ad revenue
            Adjust.trackAdRevenue(adjustAdRevenue);
            Debug.Log($"EVENT IronSource Adjust {revenue}");
        }

        #endregion




        private IEnumerator WaitToReload()
        {
            yield return _waitTime;
            LoadInterstitial();
        }
    }
}

#if UNITY_IOS

namespace AudienceNetwork
{
    public static class AdSettings
    {
        [DllImport("__Internal")]
        private static extern void FBAdSettingsBridgeSetAdvertiserTrackingEnabled(bool advertiserTrackingEnabled);

        public static void SetAdvertiserTrackingEnabled(bool advertiserTrackingEnabled)
        {
            FBAdSettingsBridgeSetAdvertiserTrackingEnabled(advertiserTrackingEnabled);
        }
    }
}

#endif