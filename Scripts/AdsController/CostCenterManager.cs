﻿namespace SourceAds
{
    public class CostCenterManager
    {
        public static void OnPaidEvent(IronSourceImpressionData impressionData)
        {
            if (impressionData == null) return;
            double revenue = impressionData.revenue??-1;
            
            Firebase.Analytics.Parameter[] AdParameters = {
                new Firebase.Analytics.Parameter("ad_platform", "ironSource"),
                new Firebase.Analytics.Parameter("ad_source", impressionData.adNetwork),
                new Firebase.Analytics.Parameter("ad_unit_name", impressionData.adUnit),
                new Firebase.Analytics.Parameter("ad_format", impressionData.adUnit),
                new Firebase.Analytics.Parameter("currency","USD"),
                new Firebase.Analytics.Parameter("value", revenue)
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_revenue_sdk", AdParameters);
        }
        
        // open_ad, native
        public static void OnPaidEvent(GoogleMobileAds.Api.AdValue impressionData, string ad_unit)
        {
            if (impressionData == null) return;
            double revenue = impressionData.Value / 1000000f;
            var impressionParameters = new[] {
                new Firebase.Analytics.Parameter("ad_platform", "Admob"),
                new Firebase.Analytics.Parameter("ad_source", "Admob"),
                new Firebase.Analytics.Parameter("ad_unit_name", ad_unit),
                new Firebase.Analytics.Parameter("ad_format", ad_unit),
                new Firebase.Analytics.Parameter("value", revenue),
                new Firebase.Analytics.Parameter("currency", impressionData.CurrencyCode),
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_revenue_sdk", impressionParameters);
        }
        
        const string LOG_CONTENT_LEVEL_AD_FORMAT = "ad_format";
        const string LOG_CONTENT_OPEN_NATIVE_COSTCENTER = "cc_openad_native_revenue";
        
        public static void LogAdRevenueOpenNative(string adFormat, float adValue, string currency)
        {
            Firebase.Analytics.Parameter[] AdRevenueParameters = {
                new Firebase.Analytics.Parameter(LOG_CONTENT_LEVEL_AD_FORMAT, adFormat),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterValue, adValue),
                new Firebase.Analytics.Parameter(Firebase.Analytics.FirebaseAnalytics.ParameterCurrency, currency)
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent(LOG_CONTENT_OPEN_NATIVE_COSTCENTER, AdRevenueParameters);
        }
    }
}