﻿public class RewardEvents
{
    public static event System.Action onAdShowFailed;
    public static event System.Action onAdOpened;
    public static event System.Action onAdClosed;
    public static event System.Action onAdRewarded;
    public static event System.Action onAdClicked;
    public static event System.Action onAdAvailable;
    public static event System.Action onAdUnavailable;
    public static event System.Action onAdLoadFailed;
    public static event System.Action onAdReady;
    
    public static void OnAdShowFailed()
    {
        onAdShowFailed?.Invoke();
    }
    
    public static void OnAdOpened()
    {
        onAdOpened?.Invoke();
    }
    
    public static void OnAdClosed()
    {
        onAdClosed?.Invoke();
    }
    
    public static void OnAdRewarded()
    {
        onAdRewarded?.Invoke();
    }
    
    public static void OnAdClicked()
    {
        onAdClicked?.Invoke();
    }
    
    public static void OnAdAvailable()
    {
        onAdAvailable?.Invoke();
    }
    
    public static void OnAdUnavailable()
    {
        onAdUnavailable?.Invoke();
    }
    
    public static void OnAdLoadFailed()
    {
        onAdLoadFailed?.Invoke();
    }
    
    public static void OnAdReady()
    {
        onAdReady?.Invoke();
    }
    
    public static void Clear()
    {
        onAdShowFailed = null;
        onAdOpened = null;
        onAdClosed = null;
        onAdRewarded = null;
        onAdClicked = null;
        onAdAvailable = null;
        onAdUnavailable = null;
        onAdLoadFailed = null;
        onAdReady = null;
    }
}
