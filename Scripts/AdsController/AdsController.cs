using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;


namespace SourceAds
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using com.adjust.sdk;
    using Firebase.Analytics;
    using GoogleMobileAds.Api;
    using GoogleMobileAds.Common;
    using UnityEngine;

    public enum AdsState
    {
        AdsProduction,
        UnlockAll
    }

    public class AdsController : MonoBehaviour
    {
        private bool _firstTimeBanner = true;
        private List<IAdsController> _adsControllers;
        [HideInInspector] public bool allowShowBanner;
        private bool _isReady;

        // For cost center
        private List<IronSourceImpressionData> _listImpressionData = new List<IronSourceImpressionData>();
        private List<AdValue> _admobImpression = new List<AdValue>();
        private bool _readyToSendIronSource = false;
        private bool _readyToSendAdmob = false;

        private void Update()
        {
            if (_readyToSendIronSource)
            {
                if (_listImpressionData.Count > 0)
                {
                    IronSourceImpressionData impressionData = _listImpressionData[0];
                    CostCenterManager.OnPaidEvent(impressionData);
                    _listImpressionData.RemoveAt(0);
                }
                else
                {
                    _readyToSendIronSource = false;
                }
            }

            if (_readyToSendAdmob)
            {
                if (_admobImpression.Count > 0)
                {
                    AdValue adValueAdmob = _admobImpression[0];
                    CostCenterManager.OnPaidEvent(adValueAdmob, "app_open");
                    CostCenterManager.LogAdRevenueOpenNative("open", adValueAdmob.Value / 1000000f,
                        adValueAdmob.CurrencyCode);
                    _admobImpression.RemoveAt(0);
                }
                else
                {
                    _readyToSendAdmob = false;
                }
            }
        }

        public bool IsReady()
        {
            return _isReady;
        }


        #region Impression Data Tracking

        public void AddImpressionDataIronSource(IronSourceImpressionData impressionData)
        {
            if (impressionData == null) return;
            _listImpressionData.Add(impressionData);
            _readyToSendIronSource = true;
        }

        public void AddImpressionDataAdmob(AdValue impressionData)
        {
            if (impressionData == null) return;
            _admobImpression.Add(impressionData);
            _readyToSendAdmob = true;
        }

        #endregion

        #region Init

        public void Init()
        {
            Debug.Log("Init AdsController");
            _adsControllers = new List<IAdsController>();
            foreach (Transform ads in transform)
            {
                if (!ads.gameObject.activeInHierarchy) continue;
                IAdsController ia = ads.GetComponent<IAdsController>();
                if (_adsControllers.Count > 0)
                {
                    _adsControllers[_adsControllers.Count - 1].SetNext(ia);
                }

                _adsControllers.Add(ia);
                ia.Init(this, () =>
                {
                    if (ads.TryGetComponent(out IronSourceManager ironSource))
                    {
                        BannerEventRegister();
                        InterstitialEventRegister();
                        RewardEventRegister();
                    }
                });
            }

            MobileAds.Initialize(initStatus =>
            {
                _isReady = true;
                LoadAppOpenAd();
                AppStateEventNotifier.AppStateChanged += OnAppStateChanged;
            });

            try
            {
                LoadRule();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public void SetOnTop(string adNetworkName) // Set ad network to use
        {
            List<IAdsController> list = new List<IAdsController>();
            foreach (var ads in _adsControllers)
            {
                if (ads.ToString().Contains(adNetworkName))
                {
                    list.Insert(0, ads);
                }
                else list.Add(ads);
            }

            _adsControllers.Clear();
            _adsControllers = list;
            for (int i = 0; i < _adsControllers.Count - 1; i++)
            {
                _adsControllers[i].SetNext(_adsControllers[i + 1]);
                Debug.Log($"New {i} {_adsControllers[i]}");
            }
        }

        #endregion

        #region Banner

        public void ShowBanner()
        {
            allowShowBanner = true;
            if (_firstTimeBanner)
            {
                LoadBanner();
                _firstTimeBanner = false;
            }

            _adsControllers[0].ShowBanner();
        }

        public void LoadBanner()
        {
            _adsControllers[0].LoadBanner(BannerType.BANNER);
        }

        public void HideBanner()
        {
            allowShowBanner = false;
            _adsControllers[0].HideBanner();
        }

        public void BannerOnAdLoadedEvent()
        {
            if (allowShowBanner) ShowBanner();
            else HideBanner();
        }

        public void BannerEventRegister()
        {
            BannerEvents.onAdLoaded += BannerOnAdLoadedEvent;
        }

        #endregion

        #region Interstitial

        public bool IsReadyShowInterstitial()
        {
            return _adsControllers[0].IsInterstitialReady();
        }

        public bool ShowInterstitial(bool allowDelay = false, Action<bool> onShow = null)
        {
// #if UNITY_EDITOR
//             onShow?.Invoke(true);
//             return true;
// #endif
            if (!IsAllowShowInterstitial()) return false;
            if (!IsReadyShowInterstitial() && !allowDelay) return false;
            // if allow delay or interstitial ready
            if (IsReadyShowInterstitial())
            {
                _allowShowOpenAds = false;
                return _adsControllers[0].ShowInterstitial(onShow);
            }

            StartCoroutine(WaitToInterstitialReady());
            return false;
        }

        public void LoadInterstitial()
        {
            _adsControllers[0].LoadInterstitial();
        }

        public void InterstitialOnAdShowSucceededEvent()
        {
            FirebaseManager.Instance.LogEvent("ADS_INTERSTITIAL");
        }

        public void InterstitialOnAdOpenedEvent()
        {
            AudiosManager.Instance.ChangePitch(0);
            Time.timeScale = 0;
        }

        public void InterstitialOnAdClosedEvent()
        {
            Time.timeScale = 1;
            AudiosManager.Instance.ChangePitch(1);
            _allowShowOpenAds = true;
            LoadInterstitial();
            PrefInfo.SetTimeShowAds();
        }

        public void InterstitialEventRegister()
        {
            InterstitialEvents.onAdShowSucceeded += InterstitialOnAdShowSucceededEvent;
            InterstitialEvents.onAdOpened += InterstitialOnAdOpenedEvent;
            InterstitialEvents.onAdClosed += InterstitialOnAdClosedEvent;
        }

        #endregion

        #region Reward

        public void ShowReward(Action<bool> callback = null, bool allowDelay = false)
        {
#if UNITY_EDITOR
            callback?.Invoke(true);
#endif
            if (MasterControl.Instance.adsState == AdsState.UnlockAll)
            {
                callback?.Invoke(true);
                Debug.Log("Unlock ad");
            }

            // if allow delay
            if (_adsControllers[0].IsRewardedVideoReady())
            {
                _allowShowOpenAds = false;
            }

            var success = _adsControllers[0].ShowRewardedVideo(callback);

            if (success) return;
            if (!allowDelay) return;
            StartCoroutine(WaitToRewardedVideoReady());
        }

        public void LoadReward()
        {
            _adsControllers[0].LoadRewardedVideo();
        }

        public void RewardedVideoOnAdOpenedEvent()
        {
            Time.timeScale = 0;
            AudiosManager.Instance.ChangePitch(0);
        }

        public void RewardedVideoOnAdClosedEvent()
        {
            FirebaseManager.Instance.LogEvent("ADS_REWARD");
            Time.timeScale = 1;
            AudiosManager.Instance.ChangePitch(1);
            _allowShowOpenAds = true;
            //ShowOpenAppAds();
        }

        public bool IsRewardAdAvailable()
        {
            return IronSource.Agent.isRewardedVideoAvailable();
        }

        public void RewardEventRegister()
        {
            RewardEvents.onAdOpened += RewardedVideoOnAdOpenedEvent;
            RewardEvents.onAdClosed += RewardedVideoOnAdClosedEvent;
        }

        #endregion

        #region OpenAds

        public string openAdsIDAndroid;
        public string openAdsIDIos;
        public bool testOpenAds;
        public bool showInStart;

        private AppOpenAd _appOpenAd;
        private DateTime _loadTime;
        private bool _openAdReadyToShow;
        public bool _allowShowOpenAds = true;
        private bool _isShowingAd;

        private Action<bool> _openAdCallback;

        private bool IsAdAvailable =>
            _appOpenAd != null && _openAdReadyToShow && (DateTime.UtcNow - _loadTime).TotalHours < 4;


        public string openAdsAndroidTestID = "ca-app-pub-3940256099942544/3419835294";

        //[BoxGroup("MobileAds-OpendAds")]
        public string openAdsIOSTestID = "ca-app-pub-3940256099942544/5662855259";


        private void OnAppStateChanged(AppState state)
        {
            // Display the app open ad when the app is foregrounded.
            Debug.Log("App State is " + state);
            if (state.Equals(AppState.Foreground))
            {
                if (showInStart && SceneManager.GetActiveScene().name.Equals("Loading"))
                {
                    Debug.Log("Show in start is true");
                    return;
                }

                ShowOpenAppAds(b => { });
            }
        }

        public void LoadAppOpenAd()
        {
            Debug.Log("Open Ad is loading");
            var id = openAdsIDAndroid;
#if UNITY_IOS
            id = openAdsIDIos;
#endif
            if (testOpenAds)
            {
#if PLATFORM_ANDROID
                Debug.LogWarning("Is using test open Ads Android");
                id = openAdsAndroidTestID;
#endif
#if PLATFORM_IOS
                Debug.LogWarning("Is using test open Ads IOS");
                id = openAdsIOSTestID;
#endif
            }

            // Create our request used to load the ad.

            // send the request to load the ad.
            Debug.Log("Load Open Ad id " + id);
            AppOpenAd.Load(id, CreateAdRequest(), (ad, error) =>
            {
                if (error != null || ad == null)
                {
                    Debug.LogError("app open ad failed to load an ad " +
                                   "with error : " + error);

                    // Gets the domain from which the error came.
                    string domain = error.GetDomain();
                    Debug.Log("Domain request " + domain);

                    // Gets the error code. See
                    // https://developers.google.com/android/reference/com/google/android/gms/ads/AdRequest
                    // and https://developers.google.com/admob/ios/api/reference/Enums/GADErrorCode
                    // for a list of possible codes.
                    int code = error.GetCode();
                    Debug.Log("Code request " + code);
                    // Gets an error message.
                    // For example "Account not approved yet". See
                    // https://support.google.com/admob/answer/9905175 for explanations of
                    // common errors.
                    string message = error.GetMessage();
                    Debug.Log("Message request " + message);

                    // Gets the cause of the error, if available.
                    AdError underlyingError = error.GetCause();
                    Debug.Log("Underlying request " + underlyingError);
                    // All of this information is available via the error's toString() method.
                    Debug.Log("Load error string: " + error);

                    // Get response information, which may include results of mediation requests.
                    ResponseInfo responseInfo = error.GetResponseInfo();
                    Debug.Log("Response info: " + responseInfo);
                    return;
                }

                Debug.Log("App open ad loaded");
                _openAdReadyToShow = true;
                _loadTime = DateTime.UtcNow;
                _appOpenAd?.Destroy();
                _appOpenAd = ad;
            });
        }


        public bool ShowOpenAppAds(Action<bool> callback = null)
        {
            Common.Log("Trying to open App");

            if (!PrefInfo.IsUsingAd())
            {
                callback?.Invoke(true);
                Debug.Log("Unlock version");
                return true;
            }

            if (_isShowingAd)
            {
                Common.Log("Is showing ad");
                return false;
            }

            if (!_allowShowOpenAds)
            {
                _allowShowOpenAds = true;
                Common.Log("Not allow show open ad");
                return false;
            }

            if (!IsAdAvailable)
            {
                Common.Log("Open ad is not loaded yet");
                return false;
            }

            showInStart = false;
            if (_appOpenAd == null)
            {
                Debug.Log("open ads is loaded yet");
                LoadAppOpenAd();
                return false;
            }

            //if (!MasterControl.Instance.adsController.is) return false;

            #region Block Ad Background

            try
            {
                BgAdBlock.Instance.Show();
            }
            catch
            {
            }

            #endregion

            RegisterEventAppOpenAd();
            _openAdCallback = callback;
            FirebaseManager.Instance.LogEvent("OPENAD");
            _appOpenAd.Show();
            PrefInfo.SetTimeShowAds();
            //Debug.Log("Show app open ads!!!");


            return true;
        }

        private AdRequest CreateAdRequest()
        {
            return new AdRequest();
        }

        private void RegisterEventAppOpenAd()
        {
            if (_appOpenAd == null) return;
            // _appOpenAd.OnPaidEvent -= HandlePaidEvent;
            // _appOpenAd.OnPaidEvent += HandlePaidEvent;
            // _appOpenAd.OnAdDidDismissFullScreenContent += HandleAdDidDismissFullScreenContent;
            // _appOpenAd.OnAdFailedToPresentFullScreenContent += HandleAdFailedToPresentFullScreenContent;
            // _appOpenAd.OnAdDidPresentFullScreenContent += HandleAdDidPresentFullScreenContent;
            // _appOpenAd.OnAdDidRecordImpression += HandleAdDidRecordImpression;

            _appOpenAd.OnAdPaid -= OnPaidEvent;
            _appOpenAd.OnAdPaid += OnPaidEvent;
            _appOpenAd.OnAdImpressionRecorded -= OnAdDidRecordImpression;
            _appOpenAd.OnAdFullScreenContentClosed += OnAdFullScreenContentClosed;
            _appOpenAd.OnAdFullScreenContentOpened += OnAdFullScreenContentOpened;
            _appOpenAd.OnAdFullScreenContentFailed += OnAdFullScreenContentFailed;
        }


        #region old ver

        private void HandlePaidEvent(object sender, AdValueEventArgs args)
        {
            Debug.LogFormat("Received paid event. (currency: {0}, value: {1})",
                args.AdValue.CurrencyCode, args.AdValue.Value);
            OnPaidEvent(args);
            //AddImpressionDataAdmob(args);
        }

        private void OnPaidEvent(AdValueEventArgs impressionData)
        {
            if (impressionData == null) return;
            double revenue = impressionData.AdValue.Value / 1000000f;
            var imp = new[]
            {
                new Firebase.Analytics.Parameter("ad_platform", "Admob"),
                new Firebase.Analytics.Parameter("ad_source", "Admob"),
                new Firebase.Analytics.Parameter("ad_unit_name", "open_ads"),
                new Firebase.Analytics.Parameter("ad_format", "open_ads"),
                new Firebase.Analytics.Parameter("value", revenue),
                new Firebase.Analytics.Parameter("currency", impressionData.AdValue.CurrencyCode)
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression", imp);
            Debug.Log(impressionData);


            AdjustAdRevenue adjustEvent = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
            //most important is calling setRevenue with two parameters
            adjustEvent.setRevenue(revenue, impressionData.AdValue.CurrencyCode);
            adjustEvent.setAdRevenueNetwork(_appOpenAd.GetResponseInfo().GetLoadedAdapterResponseInfo().AdSourceName);
            //Sent event to Adjust server
            Adjust.trackAdRevenue(adjustEvent);
        }

        private void HandleAdDidDismissFullScreenContent(object sender, EventArgs args)
        {
            Debug.Log("Closed app open ad");
            Time.timeScale = 1;
            _isShowingAd = false;
            PrefInfo.SetTimeShowAds();
            LoadAppOpenAd();

            _openAdCallback?.Invoke(true);
            _openAdCallback = null;
            try
            {
                BgAdBlock.Instance.Hide();
            }
            catch
            {
            }
        }
        
        private void HandleAdDidPresentFullScreenContent(object sender, EventArgs args)
        {
            Debug.Log("Displayed app open ad");
            //Time.timeScale = 0;
            _isShowingAd = true;
        }

        private void HandleAdFailedToPresentFullScreenContent(object sender, AdError args)
        {
            Debug.LogFormat("Failed to present the ad (reason: {0})", args.GetMessage());
            _isShowingAd = false;
            Util.Delay(1f, LoadAppOpenAd);
        }

        private void HandleAdDidRecordImpression(object sender, EventArgs args)
        {
            Debug.Log("Recorded ad impression");
        }

        #endregion

        #region new ver

        private void OnAdFullScreenContentFailed(AdError adError)
        {
            Debug.LogFormat("Failed to present the ad (reason: {0})", adError.GetMessage());
            _isShowingAd = false;
            Util.Delay(1f, LoadAppOpenAd);
        }

        private void OnAdFullScreenContentOpened()
        {
            Debug.Log("Displayed app open ad");
            //Time.timeScale = 0;
            _isShowingAd = true;
        }

        private void OnAdFullScreenContentClosed()
        {
            Debug.Log("Closed app open ad");
            Time.timeScale = 1;
            _isShowingAd = false;
            PrefInfo.SetTimeShowAds();
            LoadAppOpenAd();

            _openAdCallback?.Invoke(true);
            _openAdCallback = null;
            try
            {
                BgAdBlock.Instance.Hide();
            }
            catch
            {
            }
        }

        private void OnAdDidRecordImpression()
        {
            Debug.Log("Recorded ad impression");
        }

        private void OnPaidEvent(AdValue adValue)
        {
            if (adValue == null) return;
            AddImpressionDataAdmob(adValue);
            double revenue = adValue.Value / 1000000f;
            var imp = new[]
            {
                new Parameter("ad_platform", "Admob"),
                new Parameter("ad_source", "Admob"),
                new Parameter("ad_unit_name", "open_ads"),
                new Parameter("ad_format", "open_ads"),
                new Parameter("value", revenue),
                new Parameter("currency", adValue.CurrencyCode)
            };
            FirebaseAnalytics.LogEvent("ad_impression", imp);
            Debug.Log("ad_impression " + adValue.Value);

            var adjustEvent = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
            //most important is calling setRevenue with two parameters
            adjustEvent.setRevenue(revenue, adValue.CurrencyCode);
            //Sent event to Adjust server
            Adjust.trackAdRevenue(adjustEvent);
        }

        #endregion

        #endregion


        #region Collapsible Banner

        public String collapsibleBannerIDAndroid;
        public String collapsibleBannerIDIos;

        private BannerView _bannerView;

        private void LoadCollapsibleBanner()
        {
            Debug.Log("Creating banner view");
            String _adUnitId;

#if UNITY_ANDROID
            _adUnitId = collapsibleBannerIDAndroid;
#elif UNITY_IOS
        _adUnitId = collapsibleBannerIDIos;
#endif

            if (_bannerView == null)
            {
                createBannerView();
            }

            var adRequest = new AdRequest();
            adRequest.Extras.Add("collapsible", "bottom");

            Debug.Log("Loading ad banner");
            _bannerView.LoadAd(adRequest);


            return;

            void createBannerView()
            {
                // If we already have a banner, destroy the old one.
                if (_bannerView != null)
                {
                    DestroyBannerView();
                }

                // Create a 320x50 banner at top of the screen
                _bannerView = new BannerView(_adUnitId, AdSize.Banner, AdPosition.Bottom);
                ListenToAdEvents();
            }
        }

        public void DestroyBannerView()
        {
            if (_bannerView != null)
            {
                Debug.Log("Destroying banner view.");
                _bannerView.Destroy();
                _bannerView = null;
            }
        }

        /// <summary>
        /// listen to events the banner view may raise.
        /// </summary>
        private void ListenToAdEvents()
        {
            // Raised when an ad is loaded into the banner view.
            _bannerView.OnBannerAdLoaded += BannerOnBannerAdLoaded;
            // Raised when an ad fails to load into the banner view.
            _bannerView.OnBannerAdLoadFailed += BannerOnBannerAdLoadFailed;
            // Raised when the ad is estimated to have earned money.
            _bannerView.OnAdPaid += BannerOnAdPaid;
            // Raised when an impression is recorded for an ad.
            _bannerView.OnAdImpressionRecorded += BannerOnAdImpressionRecorded;
            // Raised when a click is recorded for an ad.
            _bannerView.OnAdClicked += BannerOnAdClicked;
            // Raised when an ad opened full screen content.
            _bannerView.OnAdFullScreenContentOpened += BannerOnAdFullScreenContentOpened;
            // Raised when the ad closed full screen content.
            _bannerView.OnAdFullScreenContentClosed += BannerOnAdFullScreenContentClosed;
        }

        private void BannerOnBannerAdLoaded()
        {
            Debug.Log("Banner view loaded an ad with response : "
                      + _bannerView.GetResponseInfo());
            BannerEvents.OnAdLoaded();
        }

        private void BannerOnBannerAdLoadFailed(LoadAdError error)
        {
            Debug.LogError("Banner view failed to load an ad with error : "
                           + error);
            BannerEvents.OnAdLoadFailed();
        }

        private void BannerOnAdPaid(AdValue adValue)
        {
            Debug.Log(String.Format("Banner view paid {0} {1}.",
                adValue.Value,
                adValue.CurrencyCode));
            
            // AddImpressionDataAdmob(adValue);
            double revenue = adValue.Value / 1000000f;
            var imp = new[]
            {
                new Parameter("ad_platform", "Admob"),
                new Parameter("ad_source", "Admob"),
                new Parameter("ad_unit_name", "collapsible_banner"),
                new Parameter("ad_format", "collapsible_banner"),
                new Parameter("value", revenue),
                new Parameter("currency", adValue.CurrencyCode)
            };
            FirebaseAnalytics.LogEvent("ad_impression", imp);
            Debug.Log("ad_impression " + adValue.Value);

            var adjustEvent = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
            //most important is calling setRevenue with two parameters
            adjustEvent.setRevenue(revenue, adValue.CurrencyCode);
            //Sent event to Adjust server
            Adjust.trackAdRevenue(adjustEvent);
        }

        private void BannerOnAdImpressionRecorded()
        {
            Debug.Log("Banner view recorded an impression.");
        }

        private void BannerOnAdClicked()
        {
            Debug.Log("Banner view was clicked.");

            BannerEvents.OnAdClicked();
        }

        private void BannerOnAdFullScreenContentOpened()
        {
            Debug.Log("Banner view full screen content opened.");
            BannerEvents.OnAdScreenPresented();
        }

        private void BannerOnAdFullScreenContentClosed()
        {
            Debug.Log("Banner view full screen content closed.");
            BannerEvents.OnAdScreenDismissed();
        }

        public void ShowCollapsibleBanner()
        {
            if (_bannerView == null)
            {
                LoadCollapsibleBanner();
            }
            else
            {
                _bannerView.Show();
            }
        }
        
        public void HideCollapsibleBanner()
        {
            if (_bannerView != null)
            {
                _bannerView.Hide();
            }
        }
        
        #endregion

        #region Wait ads function

        private IEnumerator WaitToInterstitialReady()
        {
            yield return new WaitUntil(IsAllowShowInterstitial);

            ShowInterstitial();
        }

        private IEnumerator WaitToRewardedVideoReady()
        {
            Debug.Log("wait to load ad");
            yield return new WaitUntil(IsAllowShowReward);

            ShowReward();
        }

        #endregion

        #region Rule Ads

        // remote config example
        private float adSetting_time_reward = 15f;
        private float adSetting_time_interstitial = 15f;

        private string adSetting_level = "";
        int[] adSetting_level_list;

        private string adSetting_time_normal = "";
        int[] adSetting_time_normal_list;

        public void LoadRule()
        {
            adSetting_time_reward =
                PlayerPrefs.GetFloat(AdPattern.AdSettingTimeReward, AdPattern.AdSettingTimeRewardDefault);

            adSetting_time_interstitial = PlayerPrefs.GetFloat(AdPattern.AdSettingTimeInterstitial,
                AdPattern.AdSettingTimeInterstitialDefault);

            adSetting_level = PlayerPrefs.GetString(AdPattern.AdSettingLevel, AdPattern.AdSettingLevelDefault);
            adSetting_level_list = Array.ConvertAll(adSetting_level.Split(','), int.Parse);

            adSetting_time_normal =
                PlayerPrefs.GetString(AdPattern.AdSettingTimeNormal, AdPattern.AdSettingTimeNormalDefault);
            adSetting_time_normal_list = Array.ConvertAll(adSetting_time_normal.Split(','), int.Parse);
        }

        public bool IsAllowShowInterstitial() // rule to show interstitial
        {
            try
            {
                if (!PrefInfo.IsUsingAd()) return false; // purchased no ads

                TimeSpan tp = DateTime.Now - PrefInfo.GetTimeShowAds();
                double time = tp.TotalSeconds;
                Common.Log($"time to show ad: {time}");

                /*
            // Example how to use level config to show ad, value is get from firebase config remote in FirebaseManager

            int level = PrefInfo.GetTotalUnlockedLevel(0);
            int index = 0;
            if (level < adSetting_level_list[0]) {
                return false;
            }
            else if (level >= adSetting_level_list[0] && level < adSetting_level_list[1]) {
                index = 1;
            }
            ...

            float timeRequire = adSetting_time_normal_list[index - 1];
            ...

            */

                return time >= adSetting_time_interstitial;
            }
            catch (Exception e)
            {
                //Debug.Log("Error: " + e);
                return false;
            }
        }

        public bool IsAllowShowReward()
        {
            return true;
        }

        #endregion
    }
}