﻿
public class BannerEvents
{
    public static event System.Action onAdLoaded;
    public static event System.Action onAdLeftApplication;
    public static event System.Action onAdScreenDismissed;
    public static event System.Action onAdScreenPresented;
    public static event System.Action onAdClicked;
    public static event System.Action onAdLoadFailed;
    
    public static void OnAdLoaded()
    {
        onAdLoaded?.Invoke();
    }
    
    public static void OnAdLeftApplication()
    {
        onAdLeftApplication?.Invoke();
    }
    
    public static void OnAdScreenDismissed()
    {
        onAdScreenDismissed?.Invoke();
    }
    
    public static void OnAdScreenPresented()
    {
        onAdScreenPresented?.Invoke();
    }
    
    public static void OnAdClicked()
    {
        onAdClicked?.Invoke();
    }
    
    public static void OnAdLoadFailed()
    {
        onAdLoadFailed?.Invoke();
    }
    
    public static void Clear()
    {
        onAdLoaded = null;
        onAdLeftApplication = null;
        onAdScreenDismissed = null;
        onAdScreenPresented = null;
        onAdClicked = null;
        onAdLoadFailed = null;
    }
}
