﻿
public class InterstitialEvents
{
    public static event System.Action onAdReady;
    public static event System.Action onAdLoadFailed;
    public static event System.Action onAdOpened;
    public static event System.Action onAdClosed;
    public static event System.Action onAdShowSucceeded;
    public static event System.Action onAdShowFailed;
    public static event System.Action onAdClicked;
    
    public static void OnAdReady()
    {
        onAdReady?.Invoke();
    }
    
    public static void OnAdLoadFailed()
    {
        onAdLoadFailed?.Invoke();
    }
    
    public static void OnAdOpened()
    {
        onAdOpened?.Invoke();
    }
    
    public static void OnAdClosed()
    {
        onAdClosed?.Invoke();
    }
    
    public static void OnAdShowSucceeded()
    {
        onAdShowSucceeded?.Invoke();
    }
    
    public static void OnAdShowFailed()
    {
        onAdShowFailed?.Invoke();
    }
    
    public static void OnAdClicked()
    {
        onAdClicked?.Invoke();
    }
    
    public static void Clear()
    {
        onAdReady = null;
        onAdLoadFailed = null;
        onAdOpened = null;
        onAdClosed = null;
        onAdShowSucceeded = null;
        onAdShowFailed = null;
        onAdClicked = null;
    }
}
