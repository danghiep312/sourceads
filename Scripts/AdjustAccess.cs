﻿using System;
using com.adjust.sdk;
using Sirenix.OdinInspector;
using UnityEngine;

namespace SourceAds
{
    public class AdjustAccess : MonoBehaviour
    {
        public static AdjustAccess Instance;
        private Adjust _adjust;
        public string adjustAppTokenAndroid;
        public string adjustAppTokenIOS;

        private void Awake()
        {
            Instance = this;
            FillKey();
        }
        
        public void StartManual()
        {
            AdjustConfig adjustConfig = new AdjustConfig(_adjust.appToken, _adjust.environment, (_adjust.logLevel == AdjustLogLevel.Suppress));
            adjustConfig.setLogLevel(_adjust.logLevel);
            adjustConfig.setSendInBackground(_adjust.sendInBackground);
            adjustConfig.setEventBufferingEnabled(_adjust.eventBuffering);
            adjustConfig.setLaunchDeferredDeeplink(_adjust.launchDeferredDeeplink);
            adjustConfig.setDefaultTracker(_adjust.defaultTracker);
            adjustConfig.setUrlStrategy(_adjust.urlStrategy.ToLowerCaseString());
            adjustConfig.setAppSecret(_adjust.secretId, _adjust.info1, _adjust.info2, _adjust.info3, _adjust.info4);
            adjustConfig.setDelayStart(_adjust.startDelay);
            adjustConfig.setNeedsCost(_adjust.needsCost);
            adjustConfig.setPreinstallTrackingEnabled(_adjust.preinstallTracking);
            adjustConfig.setPreinstallFilePath(_adjust.preinstallFilePath);
            adjustConfig.setAllowiAdInfoReading(_adjust.iadInfoReading);
            adjustConfig.setAllowAdServicesInfoReading(_adjust.adServicesInfoReading);
            adjustConfig.setAllowIdfaReading(_adjust.idfaInfoReading);
            adjustConfig.setCoppaCompliantEnabled(_adjust.coppaCompliant);
            adjustConfig.setPlayStoreKidsAppEnabled(_adjust.playStoreKidsApp);
            adjustConfig.setLinkMeEnabled(_adjust.linkMe);
            if (!_adjust.skAdNetworkHandling)
            {
                adjustConfig.deactivateSKAdNetworkHandling();
            }
            Adjust.start(adjustConfig);
        }
        
        
        [Button("Fill Key")]
        public void FillKey()
        {
            adjustAppTokenAndroid = adjustAppTokenAndroid.Trim();
            adjustAppTokenIOS = adjustAppTokenIOS.Trim();
            _adjust = GetComponent<Adjust>();
#if UNITY_IOS
            _adjust.startManually = true;
            _adjust.appToken = adjustAppTokenIOS;
#elif UNITY_ANDROID
            _adjust.appToken = adjustAppTokenAndroid;
#endif
            Debug.Log("_ad " + _adjust.appToken);
        }

        private void OnValidate()
        {
            _adjust = GetComponent<Adjust>();
        }
    }
}