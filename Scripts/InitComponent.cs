﻿
using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class InitComponent : MonoBehaviour
{
    [Button]
    public List<IInit> getComp()
    {
        List<IInit> list = new List<IInit>();

        foreach (var i in GetComponentsInChildren<IInit>(true))
        {
            list.Add(i);
        }

        return list;
    }

    private void Awake()
    {
        foreach (var comp in GetComponentsInChildren<IInit>(true))
        {
            comp.Init();
        }
    }


}