namespace SourceAds
{
    public interface IPluginReady
    {
        bool IsReady();
    }
}