using System;

namespace SourceAds
{
    public interface IAdsController
    {
        void Init(AdsController ctr, Action callback);

        // Banner Handler
        void LoadBanner(BannerType type);
        bool ShowBanner();
        void HideBanner();

        
        // Interstitial Handler
        void LoadInterstitial();
        bool ShowInterstitial(Action<bool> onShow);
        bool IsInterstitialReady();

        
        // Rewarded Video Handler
        void LoadRewardedVideo();
        bool ShowRewardedVideo(Action<bool> onRewarded);
        void OnRewarded();
        bool IsRewardedVideoReady();

        
        void SetNext(IAdsController ctr);
    }

}

public enum BannerType
{
    BANNER,
    LARGE,
    SMART
}