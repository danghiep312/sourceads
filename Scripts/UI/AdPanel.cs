﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        if (!PrefInfo.IsUsingAd())
        {
            gameObject.SetActive(false);
        }
    }
}
