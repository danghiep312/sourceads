﻿
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BgAdBlockLoading : MonoBehaviour, IInit
{
    public static BgAdBlockLoading Instance;
    
    public Color backGroundColor;
    private Image _image;
    private TextMeshProUGUI _text;
    
    public void Init()
    {
        Instance = this;
        _text = GetComponent<TextMeshProUGUI>();
        _image = GetComponent<Image>();
        _image.color = backGroundColor;
    }

    public void Show(string title = "", float fadeValue = 1f)
    {
        _text.text = title;
        _image.DOFade(fadeValue, 0f);
        Instance.gameObject.SetActive(true);
    }

    public void Hide()
    {
        Instance.gameObject.SetActive(false);
    }
}