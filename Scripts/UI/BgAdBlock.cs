﻿
using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class BgAdBlock : Panel, IInit
{
    public static BgAdBlock Instance;
    
    public Color backGroundColor;
    private Image _image;
    private TextMeshProUGUI _text;
    private CanvasGroup _canvas;
    
    public override void PostInit()
    {
        Instance = this;
        Debug.Log(Instance.gameObject.name + "block ad");
        _text = GetComponentInChildren<TextMeshProUGUI>();
        _image = GetComponentInChildren<Image>();
        _image.color = backGroundColor;

        _canvas = GetComponent<CanvasGroup>();
    }

    public void Show(string title = "", float fadeValue = 1f)
    {
        _canvas.alpha = 0;
        try
        {
            _text.text = title;
        }
        catch (Exception)
        {
            Debug.Log("Null text");
        }

        
        _canvas.DOFade(fadeValue, .5f);
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}