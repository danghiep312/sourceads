using UnityEngine;

[CreateAssetMenu(fileName = "BuildVer", menuName = "Asset/BuildVer")]
public class BundleVer : ScriptableObject
{
    public string buildVersionCode;
}